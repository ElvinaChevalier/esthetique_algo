// -------------------
//  Parameters and UI
// -------------------

const gui = new dat.GUI()
const params = {
    Ellipse_Size: 56,
    N_hexagone: 3,
    Random_Seed: 0,
    N_line: 20,
   
    Download_Image: () => save(),
}
gui.add(params, "Ellipse_Size", 0, 300, 1)
gui.add(params, "N_hexagone", 0, 10, 1)
gui.add(params, "Random_Seed", 0, 1000, 1)
gui.add(params, "N_line", 5, 500, 1)

// -------------------
//       Drawing
// -------------------

function draw() {
    randomSeed(params.Random_Seed)
    background('white')
    translate(width/2, height/2)
    
    for(let n=0;n<params.N_hexagone;n++){
        let pos = createVector(-(n*params.Ellipse_Size), - params.Ellipse_Size) // On commence en haut
        let dir = p5.Vector.fromAngle(- PI * 4 / 3).mult(params.Ellipse_Size) // Et donc on adapte la prochaine direction vers en bas à gauche
        for (let j = 0; j < 6*(n+1); ++j) {
            let h=pos.x;
            let w=pos.y;
            for(let i=0;i<random(5,params.N_line;i++){
                    
                const angle=random(TWO_PI)
                const signe=random()<0.5?1:-1
                line(
                    w+params.Ellipse_Size*cos(angle)/2,
                    h+params.Ellipse_Size*sin(angle)/2,
                    w+signe*params.Ellipse_Size*cos(angle)/2,
                    h-signe*params.Ellipse_Size*sin(angle)/2)
            }
            pos.add(dir)
            if(j%(n+1)==0){
                
                dir.rotate(-TWO_PI / 6)
            }
        }
    }

    endShape(CLOSE)
}

// -------------------
//    Initialization
// -------------------

function setup() {
    p6_CreateCanvas()
}

function windowResized() {
    p6_ResizeCanvas()
}