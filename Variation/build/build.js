var gui = new dat.GUI();
var params = {
    Ellipse_Size: 56,
    N_hexagone: 3,
    Random_Seed: 0,
    N_line: 5,
    Download_Image: function () { return save(); },
};
gui.add(params, "Ellipse_Size", 0, 300, 1);
gui.add(params, "N_hexagone", 0, 10, 1);
gui.add(params, "Random_Seed", 0, 1000, 1);
gui.add(params, "N_line", 5, 500, 1);
function draw() {
    randomSeed(params.Random_Seed);
    background('white');
    translate(width / 2, height / 2);
    params.N_line += second();
    params.Random_Seed += second();
    for (var n = 0; n < params.N_hexagone; n++) {
        var pos = createVector(-(n * params.Ellipse_Size), -params.Ellipse_Size);
        var dir = p5.Vector.fromAngle(-PI * 4 / 3).mult(params.Ellipse_Size);
        for (var j = 0; j < 6 * (n + 1); ++j) {
            var h = pos.x;
            var w = pos.y;
            for (var i = 0; i < random(5, params.N_line); i++) {
                var angle = random(TWO_PI);
                var signe = random() < 0.5 ? 1 : -1;
                stroke(random([
                    color(240, 150, 205),
                    color(119, 128, 217),
                    color(151, 252, 216)
                ]));
                line(w + params.Ellipse_Size * cos(angle) / 2, h + params.Ellipse_Size * sin(angle) / 2, w + signe * params.Ellipse_Size * cos(angle) / 2, h - signe * params.Ellipse_Size * sin(angle) / 2);
            }
            pos.add(dir);
            if (j % (n + 1) == 0) {
                dir.rotate(-TWO_PI / 6);
            }
        }
    }
    endShape(CLOSE);
}
function setup() {
    p6_CreateCanvas();
}
function windowResized() {
    p6_ResizeCanvas();
}
var __ASPECT_RATIO = 1;
var __MARGIN_SIZE = 25;
function __desiredCanvasWidth() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return windowWidth - __MARGIN_SIZE * 2;
    }
    else {
        return __desiredCanvasHeight() * __ASPECT_RATIO;
    }
}
function __desiredCanvasHeight() {
    var windowRatio = windowWidth / windowHeight;
    if (__ASPECT_RATIO > windowRatio) {
        return __desiredCanvasWidth() / __ASPECT_RATIO;
    }
    else {
        return windowHeight - __MARGIN_SIZE * 2;
    }
}
var __canvas;
function __centerCanvas() {
    __canvas.position((windowWidth - width) / 2, (windowHeight - height) / 2);
}
function p6_CreateCanvas() {
    __canvas = createCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
function p6_ResizeCanvas() {
    resizeCanvas(__desiredCanvasWidth(), __desiredCanvasHeight());
    __centerCanvas();
}
var p6_SaveImageSequence = function (durationInFrames, fileExtension) {
    if (frameCount <= durationInFrames) {
        noLoop();
        var filename_1 = nf(frameCount - 1, ceil(log(durationInFrames) / log(10)));
        var mimeType = (function () {
            switch (fileExtension) {
                case 'png':
                    return 'image/png';
                case 'jpeg':
                case 'jpg':
                    return 'image/jpeg';
            }
        })();
        __canvas.elt.toBlob(function (blob) {
            p5.prototype.downloadFile(blob, filename_1, fileExtension);
            setTimeout(function () { return loop(); }, 100);
        }, mimeType);
    }
};
//# sourceMappingURL=../src/src/build.js.map